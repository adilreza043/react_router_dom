import React, { Component } from 'react';
import {BrowserRouter as Router, Link, NavLink} from 'react-router-dom';
import Route from 'react-router-dom/Route';

import myimage from './image/adilpro.jpg'



// arrow funciton inside adil
const Adil=()=>{

      return (
        <div style={{background:"green",height:"400px", width:"100%"}}>
          <h1 style={{color:"white"}}>This is from the arrow functoin and </h1>
          <pre>another page</pre>
            <img height="300px" width="290px" src={myimage} alt="image_amr"/>
          </div>
      )
}
//her match is fixed..
const About=({match})=>{
  return (
    <div>
      <h3>This is: <span style={{color:"blue"}}>{match.params.about}</span></h3>
    </div>
  )
}

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <div>
            <h1>Hello Router Dom</h1>
            <hr/>
           {/* <Link to="/">Home</Link><br/>
           <Link to="/adil/">about</Link> */}
           <NavLink exact to="/" activeStyle={{color:"green"}}> Home</NavLink>
           <br/>
           <NavLink exact to="/adil/"  activeStyle={{color:"green"}}>another</NavLink>
           <br/>
           <NavLink exact to="/contact"  activeStyle={{color:"green"}}>contact</NavLink>



            <hr/>
            <div className="container">
                <h3 style={{color:"red",borderLeft:'1px solid black', borderRight:'1px solid black' }}>This is inside the container...........</h3>
                <hr />

                {/* basic routing by the inside element components */}
                {/* <Route exact path="/" render={
                  ()=>{
                    return(
                      <div><h3>this is default domain site..</h3></div>
                    )
                  }
                } />

                <Route exact path="/about" render={
                  ()=>{
                    return (
                      <div>
                        <h1>This is from the <span style={{color:"red"}}>About</span> page</h1>
                      </div>
                    )
                  }
                }/>*/}

                <Route exact path="/contact" render={
                  ()=>{
                    return (
                      <div>
                        <pre>
                          This is    : ADIL<br/>
                          department : CSE<br/>
                          varsity    : Rajshahi University Of Engineering & Technology

                        </pre>
                      </div>
                    )
                  }
                }/> 

                {/* exact and strict mode, strict parameterr matching the url exactly the every character while exact parameter not */}
                <Route path="/adil/boss/" exact strict render={
                  function(){
                    return (
                      <div>
                        <h3 style={{color:"green"}}>This is from the strict and exact </h3>
                      </div>
                    )
                  }
                } />
                  {/* this component's c should be small letter */}
                <Route  strict exact  path="/adil/" component={Adil} />
                <Route  strict exact  path="/about/:about" component={About} />


            </div>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
